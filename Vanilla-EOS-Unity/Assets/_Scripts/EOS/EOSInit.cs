﻿using UnityEngine;
using Epic.OnlineServices.Platform;

// More-Efficient Coroutines
using MEC;
using IEnumerator = System.Collections.Generic.IEnumerator<float>;

/// <summary>
/// Epic Online Services (EOS) Manager : Init
/// https://dev.epicgames.com/docs/services/en-US/CSharp/GettingStarted/index.html
/// </summary>
public static class EOSInit
{
    #region Vars
    public static EOSMgr s_eosMgr => EOSMgr.s_EosMgr;
    public static Epic.OnlineServices.Platform.PlatformInterface s_platform
    {
        get => EOSMgr.s_Platform;
        set => EOSMgr.s_Platform = value;
    }

    // ##################################################################
    // SECRETS: Start here >> https://dev.epicgames.com/portal/
    // 1. Create product
    // 2. Click game on left
    // 3. Product settings @ top-right
    // ##################################################################
    private const string secretEosProductId = "TODO";        // at #3: "Product ID" | https://i.imgur.com/5lagSOf.png
    private const string secretEosDeploymentId = "TODO";     // at #3 "Deployments" btn to the right of a "Sandbox" choice >> "New Deployment" | https://i.imgur.com/pyNjjcQ.png
    private const string secretEosClientId = "TODO";         // at #3 at very bottom of page >> "New Client" btn >> copy the NAME you created | https://i.imgur.com/PWrJ9I7.png
    private const string secretEosClientSecret = "TODO";     // at #3 at very bottom of page >> "New Client" btn >> copy the ID (secret) below your name you created | https://i.imgur.com/PWrJ9I7.png

    private const string secretEosSandboxIdDev = "TODO";     // at #3: "Sandboxes" - Dev | https://i.imgur.com/flChkFj.png
    private const string secretEosSandboxIdStaging = "TODO"; // at #3: "Sandboxes" - Staging | https://i.imgur.com/flChkFj.png
    private const string secretEosSandboxIdLive = "TODO";    // at #3: "Sandboxes" - Live | https://i.imgur.com/flChkFj.png
    #endregion /Vars


    #region Pre-Init
    // ....................................................................................
    /// <summary>Validates the integrity, ensuring no empty or TODO</summary>
    /// <returns>Returns true on valid</returns>
    public static bool CheckValidCredentials()
    {
        if (string.IsNullOrEmpty(secretEosProductId) || secretEosProductId == "TODO")
            return false;
        if (string.IsNullOrEmpty(secretEosDeploymentId) || secretEosDeploymentId == "TODO")
            return false;
        if (string.IsNullOrEmpty(secretEosClientId) || secretEosClientId == "TODO")
            return false;
        if (string.IsNullOrEmpty(secretEosClientSecret) || secretEosClientSecret == "TODO")
            return false;

        // Sandbox depends on environment
        string selectedSandboxId = getSandboxIdFromStage();
        if (string.IsNullOrEmpty(selectedSandboxId) || selectedSandboxId == "TODO")
            return false;

        // All bueno!
        return true;
    }

    // ....................................................................................
    /// <summary>
    /// Dev environment? Use Dev sandbox ID! ...etc. Set this dynamically 
    /// somewhere in your code depending on your environment.
    /// </summary>
    /// <returns></returns>
    private static string getSandboxIdFromStage()
    {
        switch (s_eosMgr.Stage)
        {
            case EOSMgr.EosStage.Dev:
                return secretEosSandboxIdDev;
            case EOSMgr.EosStage.Staging:
                return secretEosSandboxIdStaging;
            case EOSMgr.EosStage.Live:
                return secretEosSandboxIdLive;

            default:
                return null;
        }
    }
    #endregion /Pre-Init


    // ....................................................................................
    /// <summary>
    /// After created => sends callback
    /// </summary>
    /// <returns></returns>
    public static IEnumerator _InitCreate()
    {
        #region Use this if you pull your secrets from a server
        //Debug.Log($"[EOSInit] @ _InitPlatform: Awaiting Secrets");

        //while (string.IsNullOrEmpty(secretEosDeploymentId))
        //    yield return Timing.WaitForSeconds(0.1f);
        #endregion /Use this if you pull your secrets from a server

        // ----------------------
        // Init
        Debug.Log($"[EOSInit] _initSequence: Awaiting init");

        init();

        if (s_eosMgr.StopInit)
            yield break;

        s_eosMgr.OnPlatformInitd();

        // ----------------------
        // Create
        Debug.Log($"[EOSInit] _initSequence: Awaiting create");

        yield return Timing.WaitUntilDone(Timing.RunCoroutine(
            _create().CancelWith(s_eosMgr.gameObject)));

        s_eosMgr.OnPlatformCreated();
    }

    // ....................................................................................
    /// <summary>Sets 'stopInit' on err</summary>
    /// <returns></returns>
    private static void init()
    {
        //Debug.Log("[EOSInit] @ init");

        var initOpts = new InitializeOptions()
        {
            ProductName = "Your Game Name",
            ProductVersion = "v1.0"
            //SystemInitializeOptions
        };

        Epic.OnlineServices.Result result = Epic.OnlineServices.Platform
            .PlatformInterface.Initialize(initOpts);

        // Validate
        bool isFail =
            result != Epic.OnlineServices.Result.Success &&
            result != Epic.OnlineServices.Result.AlreadyConfigured;

        if (isFail)
        {
            // Fail
            Debug.LogError("[EOSInit]**ERR @ init: Failed to initialize platform");
            s_eosMgr.StopInit = true;
            //return;
        }
    }

    // ....................................................................................
    /// <summary>Sets 'stopInit' on err</summary>
    /// <returns></returns>
    private static IEnumerator _create()
    {
        //Debug.Log("[EOSInit] @ _create");

        // Create options
        var clientCredentials = new ClientCredentials();
        clientCredentials.ClientId = secretEosClientId;
        clientCredentials.ClientSecret = secretEosClientSecret;

        var createOpts = new Options()
        {
            //CacheDirectory = @"%userprofile%\appdata\LocalLow\Imperium42 Game Studio\Throne of Lies\EOS\",
            ClientCredentials = clientCredentials,
            ProductId = secretEosProductId,
            SandboxId = getSandboxIdFromStage(),
            DeploymentId = secretEosDeploymentId
            //EncryptionKey = "",
            //Flags = Epic.OnlineServices.Platform.PlatformFlags.None,
            //IsServer = false,
            //OverrideCountryCode = false,
            //OverrideLocaleCode = false,
            //Reserved = false,
            //TickBudgetInMilliseconds = 0            
        };

        // ------------------------------
        // Set singleton => This takes a sec
        s_platform = PlatformInterface.Create(createOpts);

        //yield return Timing.WaitUntilDone(Timing.RunCoroutine(
        //    _waitNotNull(s_platform).CancelWith(gameObject)));

        // ---------------------
        if (s_platform == null)
        {
            // Fail
            Debug.LogError("[EOSInit]**ERR @ create: Failed to create platform");
            s_eosMgr.StopInit = true;
            //yield break;
        }

        yield break;
    }
}
