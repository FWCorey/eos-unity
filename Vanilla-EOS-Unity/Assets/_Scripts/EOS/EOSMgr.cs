﻿using UnityEngine;
using Epic.OnlineServices.Auth;

// More-Efficient Coroutines
using MEC;
using IEnumerator = System.Collections.Generic.IEnumerator<float>;

/// <summary>
/// Epic Online Services (EOS) Manager : Main
/// https://dev.epicgames.com/docs/services/en-US/CSharp/GettingStarted/index.html
/// </summary>
public class EOSMgr : MonoBehaviour
{
    #region vars
    public static EOSMgr s_EosMgr;
    public static Epic.OnlineServices.Platform.PlatformInterface s_Platform;
    
    [HideInInspector]
    public bool StopInit = false;
    public bool IsInitd => s_Platform != null;
    public bool? IsLoggedIn = null; // null == we haven't even attempted login, yet
    public EosStage Stage;

    public enum EosStage
    {
        Dev,
        Staging,
        Live
    }
    #endregion /vars


    #region Pre-Init
    // ....................................................................................
    void Awake()
    {
        if (s_EosMgr != null)
        {
            Debug.LogError("[EOSMgr]**ERR @ Awake: Dupe instance found! Destroying.");
            Destroy(gameObject);
            return;
        }

        // Set singleton
        s_EosMgr = this;
        Stage = EosStage.Dev;
    }

    // ....................................................................................
    void Start() => Timing.RunCoroutine(_initSequence().CancelWith(gameObject));
    #endregion /Pre-Init


    #region Init
    // ....................................................................................
    /// <summary>High-level coroutine for EOS startup sequence</summary>
    /// <returns></returns>
    IEnumerator _initSequence()
    {
        Debug.Log($"[EOSMgr] @ _initSequence: Awaiting Init+Create");

        // ----------------------
        // Init
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(
            EOSInit._InitCreate().CancelWith(gameObject)));

        if (StopInit)
        {
            Debug.LogError("[EOSMgr]**ERR @ _initSequence: StopInit");
            yield break;
        }

        // => OnPlatformInitd()
        // => OnPlatformCreated()

        // ----------------------
        // Auto-Login: Toss up a portal login page
        // Optional TODO: Move login to a button || change login type from Portal
        Debug.Log($"[EOSMgr] _initSequence: Awaiting Login via '{EOSLogin.LOGIN_TYPE.ToString()}' type");

        Timing.RunCoroutine(EOSLogin._PrepLogin().CancelWith(gameObject));

        // ----------------------
        // Wait until logged in
        //yield return Timing.WaitUntilDone(Timing.RunCoroutine(
        //    EOSLogin._PrepLogin().CancelWith(gameObject)));

        // => OnLoginSuccess()
    }

    // ....................................................................................
    /// <summary>
    /// At this point, s_platform != null. 
    /// After the platform is created, tick before creating to listen for callbacks.
    /// </summary>
    public void OnPlatformInitd()
    {
        if (StopInit)
        {
            Debug.LogError("[EOSMgr]**ERR @ OnPlatformInitd: StopInit");
            return;
        }

        // Start 100ms/tick heartbeat
        Timing.RunCoroutine(startHeartbeatTickAsync().CancelWith(gameObject));
    }

    // ....................................................................................
    /// <summary>
    /// At this point, we may now login and utilize most EOS funcs
    /// </summary>
    public void OnPlatformCreated()
    {
        if (StopInit)
        {
            Debug.LogError("[EOSMgr]**ERR @ OnPlatformCreated: StopInit");
            return;
        }
    }

    // ....................................................................................
    /// <summary>Once/100ms loop</summary>
    /// <returns></returns>
    IEnumerator _tick()
    {
        // Once/100ms
        while (true)
        {
            s_Platform.Tick();
            yield return Timing.WaitForSeconds(0.1f);
        }
    }

    // ....................................................................................
    /// <summary>Sets once/100ms loop && sets 'ready'</summary>
    private IEnumerator startHeartbeatTickAsync()
    {
        Debug.Log($"[EOSMgr] @ startHeartbeatTick");

        while (!IsInitd)
            yield return Timing.WaitForSeconds(0.1f);

        // Done with init? s_platform != null
        Timing.RunCoroutine(_tick().CancelWith(gameObject));
    }
    #endregion /Init


    #region Post-Init
    // ....................................................................................
    /// <summary>
    /// This is the "successful login" async callback - we're in!
    /// </summary>
    /// <param name="callbackInfo"></param>
    public void OnLoginSuccess(LoginCallbackInfo callbackInfo)
    {
        Debug.Log($"<color=yellow>[EOSMgr] @ OnLoginSuccess ({callbackInfo.ResultCode}): " +
            $"Welcome, LocalUserId '{callbackInfo.LocalUserId}'</color>");
        
        //var clientData = callbackInfo.ClientData; // Remember that arbitrary Object we passed @ Login()?
    }

    // ....................................................................................
    /// <summary>
    /// Wondering about the #if? If we Shutdown() inside the Unity editor, it shuts down the dll, itself!
    /// So if you did this, EOS would spam with vague errors until you RESTART Unity! 
    /// As for Release(), that needs to happen each play session to prevent memory leaks. It's a dll thing.
    /// </summary>
    private void OnDestroy()
    {
        Debug.Log("[EOSMgr] @ OnDestroy");

        if (s_Platform != null)
            s_Platform.Release();

#if !UNITY_EDITOR
        Epic.OnlineServices.Platform.PlatformInterface.Shutdown();
#endif
    }
    #endregion /Post-Init


    #region Utils
    // ....................................................................................
    /// <summary>
    /// Sets stopInit on fail. Use this from an IEnumerator: 
    /// yield return Timing.WaitUntilDone(Timing.RunCoroutine(_waitNotNull().CancelWith(gameObject)));
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="nullCheck"></param>
    /// <returns></returns>
    private IEnumerator _waitNotNull<T>(T nullCheck)
    {
        short ttl = 50; // 5 secs @ 0.1f intervals
        while (nullCheck == null)
        {
            yield return Timing.WaitForSeconds(0.1f);
            ttl--;

            if (ttl < 0)
            {
                StopInit = true;
                Debug.LogError("[EOSMgr]**ERR @ _waitNotNull: Timeout after 5s");
                yield break;
            }
        }
    }
    #endregion /Utils

}
